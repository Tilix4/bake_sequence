"""
Addon Bake Sequence
GPLv3 Licence

Copyright (c) 2018 Félix David

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""

import bpy
from bpy.app.handlers import persistent
from os.path import isfile as os_isfile
from os import remove as os_remove

bl_info = {
	"name": "Bake Sequence",
	"author": "Félix David",
	"version": (1, 1),
	"blender": (2, 79, 5),
	"location": "Properties > Render",
	"description": "Bake a sequence of images using output and bake native parameters.",
	"warning": "",
	"wiki_url": "",
	"tracker_url": "",
	"category": 'Render'}


class BakeSequenceSettings(bpy.types.PropertyGroup):
	# All the bake Sequence settings.

	imagename = bpy.props.StringProperty(
		name='Baking image name',
		description='The name of the image to bake in.',
		default="Bake_Image"
	)

	imagepath = bpy.props.StringProperty(
		name='Image Path',
		description='Path to save the baked image.',
		# default="Bake_Image"
	)

	bake_next_frame = bpy.props.BoolProperty(
		name='Bake next frame',
		description='Boolean to toggle bake of next frame.',
		default=False
	)


class BakeButtonsPanel():
	bl_space_type = 'PROPERTIES'
	bl_region_type = 'WINDOW'
	bl_context = "render"

	# COMPAT_ENGINES must be defined in each subclass, external engines can add themselves here

	@classmethod
	def poll(cls, context):
		scene = context.scene
		return scene and (scene.render.engine in cls.COMPAT_ENGINES)


class OBJECT_PT_bakesequence(BakeButtonsPanel, bpy.types.Panel):
	bl_category = "Tools"
	bl_label = "Bake Sequence"
	bl_options = {'DEFAULT_CLOSED'}
	COMPAT_ENGINES = {'CYCLES'}

	def draw(self, context):
		layout = self.layout
		init = False
		for i in bpy.data.images:
			if i.name == context.scene.bs_settings.imagename:
				init = True

		if not init:
			row = layout.row()
			row.operator("object.initbaker", text="Initialize", icon='IMAGE_DATA')
		else:
			row = layout.row()
			row.operator("object.sequencebaker", text="Bake Sequence", icon='RENDER_STILL')
			row.active = bool(bpy.ops.object.bake.poll())
			row = layout.column(align=True)
			row.label("Bake image size :")
			row.prop(bpy.data.images[context.scene.bs_settings.imagename], "generated_width", text="X")
			row.prop(bpy.data.images[context.scene.bs_settings.imagename], "generated_height", text="Y")


class OBJECT_OT_sequencebaker(bpy.types.Operator):
	bl_label = "Bake Sequence Operator"
	bl_idname = "object.sequencebaker"
	bl_description = "Bake several frames into image sequence."
	bl_options = {'REGISTER'}

	# Variables
	user_frame = None
	still_running = False
	bake_init_handler = None
	bake_complete_handler = None
	bake_cancel_handler = None
	timer = None
	incompatible_format = {'AVI_JPEG', 'AVI_RAW', 'FRAMESERVER', 'FFMPEG'}

	# def __init__(self):

	def __del__(self):
		# Remove all handlers
		bpy.app.handlers.bake_init.clear()
		bpy.app.handlers.bake_complete.clear()
		bpy.app.handlers.bake_cancel.clear()

	def modal(self, context, event):
		scene = bpy.context.scene

		# Testing compatible formats
		for i in self.incompatible_format:
			if scene.render.image_settings.file_format == i:
				self.report({'INFO'}, "Bake Sequence : cannot export in video format. Please choose an image format (ex: PNG).")
				return {'CANCELLED'}

		if event.type in {'ESC'}:
			print("[Bake Sequence : exit]")
			self.report({'INFO'}, "Bake Sequence : rendering exits.")
			return {'CANCELLED'}

		if context.scene.bs_settings.bake_next_frame:
			self.set_imagepath(scene)
			if os_isfile(scene.bs_settings.imagepath) and not scene.render.use_overwrite:
				# Next Frame
				scene.frame_set(scene.frame_current + 1)
				return {'RUNNING_MODAL'}
			bpy.ops.object.bake('INVOKE_DEFAULT', type=scene.cycles.bake_type, use_clear=True)
			context.scene.bs_settings.bake_next_frame = False

		if scene.frame_current > scene.frame_end or not self.still_running:
			context.window_manager.event_timer_remove(self.timer)
			scene.frame_set(self.user_frame)
			print("[Bake Sequence : end]")
			self.report({'INFO'}, "Bake Sequence : rendering finished !")
			return {'FINISHED'}
		else:
			return {'PASS_THROUGH'}

	def invoke(self, context, event):
		scene = bpy.context.scene
		object = bpy.context.scene.objects.active
		material = object.material_slots.data.active_material

		if len(bpy.context.selected_objects) > 1:
			self.report({'INFO'}, "Bake Sequence : cannot render several objects at the same time ! Please select only one.")
			return {'CANCELLED'}


		# Testing object is able to be baked
		if not bpy.ops.object.bake.poll():
			self.report({'WARNING'}, "Active object cannot be baked. Please select a correct one.")
			return {'CANCELLED'}
		if object.data.uv_layers.active is None:
			self.report({'WARNING'}, "Missing UVs")
			return {'CANCELLED'}
		if material is None:
			self.report({'WARNING'}, "Missing material to bake")
			return {'CANCELLED'}

		# Clear functions useful for coding and debugging
		bpy.app.handlers.bake_init.clear()
		bpy.app.handlers.bake_complete.clear()
		bpy.app.handlers.bake_cancel.clear()

		# Bake handlers functions to call : the following structure allows having previous handlers from other add-ons
		# and avoids double Bake Sequence handlers - !Not working well yet!
		create_handler = True
		for h in bpy.app.handlers.bake_init:
			if self.bake_init_handler == h:
				create_handler = False
		if create_handler:
			bpy.app.handlers.bake_init.append(self.bake_init)
			self.bake_init_handler = bpy.app.handlers.bake_init[len(bpy.app.handlers.bake_init) - 1]

		create_handler = True
		for h in bpy.app.handlers.bake_complete:
			if self.bake_complete_handler == h:
				create_handler = False
		if create_handler:
			bpy.app.handlers.bake_complete.append(self.bake_complete)
			self.bake_complete_handler = bpy.app.handlers.bake_complete[len(bpy.app.handlers.bake_complete) - 1]

		create_handler = True
		for h in bpy.app.handlers.bake_cancel:
			if self.bake_cancel_handler == h:
				create_handler = False
		if create_handler:
			bpy.app.handlers.bake_cancel.append(self.bake_cancel)
			self.bake_cancel_handler = bpy.app.handlers.bake_cancel[len(bpy.app.handlers.bake_cancel) - 1]

		# Saving current frame to restore it at the end
		self.user_frame = scene.frame_current
		scene.frame_set(scene.frame_start)

		# Get image
		image = bpy.data.images[scene.bs_settings.imagename]

		# Create Node and associate the image to it
		material.use_nodes = True
		nodes = material.node_tree.nodes
		node_label = "Bake Sequence"
		create_node = True
		node = None
		for n in nodes:
			if n.label == node_label:
				create_node = False
				node = n
				break
		if create_node:
			node = nodes.new("ShaderNodeTexImage")
			node.label = node_label
		node.image = image
		node.select = True
		nodes.active = node

		context.window_manager.modal_handler_add(self)
		bpy.context.scene.bs_settings.bake_next_frame = True
		self.timer = context.window_manager.event_timer_add(0.1, context.window)
		self.still_running = True
		return {'RUNNING_MODAL'}

	def cancel(self, context):
		context.window_manager.event_timer_remove(self.timer)

	def set_imagepath(self, scene):
		# Create file path from digits
		filepath =  bpy.path.abspath(scene.render.filepath)
		digits = filepath.count("#")
		filepath = filepath.replace("#", "")
		for d in range(digits - len(str(scene.frame_current))):
			filepath = filepath + "0"
		if scene.render.use_file_extension:
			scene.bs_settings.imagepath = filepath + str(scene.frame_current) + scene.render.file_extension
		else:
			scene.bs_settings.imagepath = filepath + str(scene.frame_current)

	# Actions to do at bake events
	@persistent
	def bake_init(self, scene):
		print("[Bake Sequence : frame " + str(scene.frame_current) + " starting]")
		if scene.render.use_placeholder:
			bpy.ops.object.setplaceholder()
		self.report({'INFO'}, "Bake Sequence : frame " + str(scene.frame_current) + " is starting !")

	@persistent
	def bake_complete(self, scene):
		print("[Bake Sequence : frame " + str(scene.frame_current) + " complete]")
		self.report({'INFO'}, "Bake Sequence : frame " + str(scene.frame_current) + " complete !")
		if self.still_running:
			# Image saving
			image = bpy.data.images[bpy.context.scene.bs_settings.imagename]
			image.save_render(scene.bs_settings.imagepath)
			print("[Bake Sequence : Save image -> " + scene.bs_settings.imagepath + "]")

			# Next Frame
			scene.frame_set(scene.frame_current + 1)

		if scene.frame_current <= scene.frame_end:
			bpy.context.scene.bs_settings.bake_next_frame = True

		scene.update()

	@persistent
	def bake_cancel(self, scene):
		os_remove(scene.bs_settings.imagepath)
		self.still_running = False
		print("[Bake Sequence : cancelled]")


class OBJECT_OT_initbaker(bpy.types.Operator):
	bl_label = "Initialize Bake Sequence Operator"
	bl_idname = "object.initbaker"
	bl_description = "Creates the image to bake into."
	bl_options = {'REGISTER'}

	def execute(self, context):
		scene = bpy.context.scene
		createImage = True
		for img in bpy.data.images:
			if img.name == "Bake_Image":
				createImage = False
				scene.bs_settings.imagename = img.name
				break

		if createImage:
			print("[Bake Sequence : " + scene.bs_settings.imagename + " created]")
			self.report({'INFO'}, "Bake Sequence : " + scene.bs_settings.imagename + " created")
			img = bpy.data.images.new("Bake_Image", alpha=True,
									  width=1024,
									  height=1024)
			img.generated_type = "UV_GRID"

		# Set the image in the open UV/Image Editor
		set_area = True
		for area in bpy.context.screen.areas:
			if area.type == 'IMAGE_EDITOR':
				area.spaces.active.image = bpy.data.images[scene.bs_settings.imagename]
				set_area = False
				break
		if set_area:
			area_id = len(bpy.context.screen.areas) - 1
			if bpy.context.screen.areas[area_id].type == '3D_VIEW':
				area_id -= 1
				print(bpy.context.screen.areas[area_id].type)
			if area_id > 4:
				bpy.context.screen.areas[area_id].type = 'IMAGE_EDITOR'
				bpy.context.screen.areas[area_id].spaces.active.image = bpy.data.images[scene.bs_settings.imagename]

		self.report({'INFO'}, "Bake Sequence initialized")
		return {'FINISHED'}


class OBJECT_OT_setplaceholder(bpy.types.Operator):
	bl_label = "Set Place Holder Operator"
	bl_idname = "object.setplaceholder"
	bl_description = "Write a place holder at a specific path"
	bl_options = {"REGISTER", "UNDO"}

	def execute(self, context):
		scene = context.scene
		img = bpy.data.images.new("placeholder", alpha=True,
								  width=1,
								  height=1)
		img.save_render(scene.bs_settings.imagepath)
		bpy.data.images.remove(img)
		return {'FINISHED'}


def register():
	bpy.utils.register_module(__name__)
	bpy.types.Scene.bs_settings = \
		bpy.props.PointerProperty(type=BakeSequenceSettings)


def unregister():
	bpy.utils.unregister_module(__name__)

	del bpy.types.Scene.bs_settings


if __name__ == "__main__":
	register()
