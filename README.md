### Bake Sequence *Add-on for Blender*

## Introduction
The purpose of Bake Sequence is to allow the user to render an image sequence of baked textures.
It comes from a need in realtime mapping softwares, where playing an animated texture is very useful, saving time of several renders from different points of view. It is also more flexible for on-set last-minute changes.  
The core operation is based on *Bake Handlers*, not implemented in Blender yet.

__For now it works only with one custom build of Blender 2.79.5 based on the [D3533](https://developer.blender.org/D3533) Blender Phabricator's thread, submitted by Élie Michel. You can download this build [here](http://felixdavid.com/share/blender-2.79.5-bake-handlers-branch.zip).__

## How to install
With the `Install Add-on from File...` button in the **Blender User Preferences** window. There is only one file to select : *bake_sequence.py*

## How to use
**Bake Sequence** works only with Cycles, then you have to check you're in the correct render engine to see it appear.  
1. The tab is located in the bottom of the *Render* window. You have to initialize it first. It creates a generated image "Bake_Image" which will be used for the baking.

  ![alt text](./pictures/bs_tab.png)  

2. After initialization, you'll see the starting button and the size settings for the *Bake_Image*. There is nothing more than clicking on the button to start rendering.  
*NB: If you have no UV_Map or no active material, the process will stop and a message will warn you about what's missing.*  

  ![alt text](./pictures/bs_tab_initialized.png)
3. Export settings of baked images are set with the native **Output** tab.  
*NB: `Cache Result` doesn't work with this add-on, because it has no sense with baking*  

  ![alt text](./pictures/output_tab.png)
